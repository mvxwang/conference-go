import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    message = json.loads(body.decode())
    presenter_email = message["presenter_email"]
    presenter_name = message["presenter_name"]
    title = message["presenter_title"]
    subject = "Your presentation has been accepted"
    body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"
    send_mail(
        subject,
        body,
        "admin@conference.go",
        [presenter_email],
        fail_silently=False,
    )
    print("Approval email has been sent.")


def process_rejection(ch, method, properties, body):
    message = json.loads(body.decode())
    presenter_email = message["presenter_email"]
    presenter_name = message["presenter_name"]
    title = message["presenter_title"]
    subject = "Your presentation has been rejected"
    body = f"{presenter_name}, we're sorry to tell you that your presentation {title} has been rejected"
    send_mail(
        subject,
        body,
        "admin@conference.go",
        [presenter_email],
        fail_silently=False,
    )
    print("Rejection email has been sent.")


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()
